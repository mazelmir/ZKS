/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.zks;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

/**
 *
 * @author Karel
 */
public class SampleTest {

    private static FirefoxDriver driver;
    private static String baseUrl;
    private StringBuffer verificationErrors = new StringBuffer();

    public SampleTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        // https://github.com/mozilla/geckodriver
        System.setProperty("webdriver.gecko.driver", "./geckodriver");

        driver = new FirefoxDriver();
        baseUrl = "http://demo.redmine.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {
        driver.quit();
        if (verificationErrors.length() > 0) {
            fail(verificationErrors.toString());
        }
    }

    @Test
    public void hello() {
        driver.get(baseUrl);
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
}
