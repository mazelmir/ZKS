package cz.cvut.zks;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.util.*;

public class WebDriverPool {

    private Map<String, WebDriver> drivers = new HashMap<String, WebDriver>();
    private List<WebDriver> driversInUse = new ArrayList<WebDriver>();

    public WebDriverPool() {
        Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
            public void run(){
                for (WebDriver driver : drivers.values())
                    driver.close();

                if (!driversInUse.isEmpty())
                    throw new IllegalStateException("There are still drivers in use, did someone forget to return it? (size: " + driversInUse.size() + ")");
            }
        });
    }

    private WebDriver createFirefoxDriver(Locale locale){
        FirefoxOptions options = new FirefoxOptions()
                .addPreference("intl.accept_languages", formatLocale(locale));
        return new FirefoxDriver(options);
    }

    private String formatLocale(Locale locale) {
        return locale.getCountry().length() == 0
                ? locale.getLanguage()
                : locale.getLanguage() + "-" + locale.getCountry().toLowerCase();
    }

    /**
     * @param clazz
     * @param locale
     * @return web driver which can be new or recycled
     */
    public synchronized WebDriver getWebDriver(Class<? extends WebDriver> clazz, Locale locale){

        String key = clazz.getName() + "-" + locale;

        if(!drivers.containsKey(key)){

            if(clazz == FirefoxDriver.class){
                drivers.put(key, createFirefoxDriver(locale));
            }

            // TODO create other drivers here ...

            // else if(clazz == ChromeDriver.class){
            //     drivers.put(key, createChromeDriver(locale));
            // }

            else{
                throw new IllegalArgumentException(clazz.getName() + " not supported yet!");
            }
        }

        WebDriver driver = drivers.get(key);

        if(driversInUse.contains(driver))
            throw new IllegalStateException("This driver is already in use. Did someone forgot to return it?");

        driversInUse.add(driver);
        return driver;
    }

    public synchronized void returnWebDriver(WebDriver driver){
        driversInUse.remove(driver);
    }
}
