package cz.cvut.zks;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.junit.Assert.assertTrue;

//Be careful in splitting the test code to individual @Tests:
//an execution plan of Tests is not guaranteed by default
//We need to use @FixMethodOrder(MethodSorters.{NAME_ASCENDING}) above unit test class declaration
//http://junit.org/apidocs/index.html?org/junit/runners/MethodSorters.html
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestProstredi {

    static RemoteWebDriver driver;

    @BeforeClass
    public static void beforeClass() {
        // System.setProperty("webdriver.chrome.driver", "C:\\chromedriver\\chromedriver.exe");
        // driver = new ChromeDriver();
        System.setProperty("webdriver.gecko.driver", "./geckodriver");


        FirefoxOptions options = new FirefoxOptions()
                .addPreference("intl.accept_languages", "en-US");
        driver = new FirefoxDriver(options);

        //driver = new FirefoxDriver();
    }

    @AfterClass
    public static void afterClass() {
        //Pause.pause(2);
        driver.quit();
    }

    @Test
    public void step1_loginTest() {
        driver.get("http://demo.redmine.org");
        driver.findElement(By.linkText("Sign in")).click();

        waitForElement(driver, By.id("username"));

        driver.findElement(By.id("username")).sendKeys("tinman");
        driver.findElement(By.id("password")).sendKeys("testit135");
        driver.findElement(By.name("login")).click();
    }

    @Test
    public void step2_projectTest() {
        // FIND TESTING PROJECT
        driver.findElement(By.linkText("Projects")).click();
        driver.findElement(By.linkText("TestingProjectTraining")).click();

        // CHECK NUMBER OF BUGS
        WebElement bugsLinkElement = driver.findElement(By.linkText("Bug"));
        WebElement bugsStatsElement = bugsLinkElement.findElement(By.xpath("parent::node()"));
        String bugsStatusString = bugsStatsElement.getText();
        String totalBugsCount = bugsStatusString.substring(bugsStatusString.indexOf("/") + 1).trim();
        int totalBugsCountNumber = (new Integer(totalBugsCount)).intValue();

        // JUnit style assert
        assertTrue("Log message: totalBugsCountNumber is less than 20", totalBugsCountNumber > 20);

        //fail();
    }

    // try to set alwaysRun false and fail previous test
    //@Test (dependsOnMethods={"projectTest"})
    @Test
    public void step3_logoutTest() {
        // LOG OUT
        //Log.log("executing logout");
        driver.findElement(By.linkText("Sign out")).click();
    }

    @Test
    public void step4_incorrect_loginTest() {
        driver.get("http://demo.redmine.org");
        driver.findElement(By.linkText("Sign in")).click();

        waitForElement(driver, By.id("username"));

        driver.findElement(By.id("username")).sendKeys("tinman");
        driver.findElement(By.id("password")).sendKeys("incorrectpassword");
        driver.findElement(By.name("login")).click();

        List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + "Invalid user or password" + "')]"));
        assertTrue("Text not found!", list.size() > 0);
    }

    private void waitForElement(RemoteWebDriver driver, final By by) {
        Wait<WebDriver> wait = new WebDriverWait(driver, 10);
        wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return driver.findElement(by).isDisplayed();
            }
        });
    }

}
